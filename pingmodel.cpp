﻿#include "pingmodel.h"
#include <QString>
#include <QDebug>
#include <QTextCodec>

PingModel::PingModel(QObject *parent) :
    QObject(parent), running(false)
{
    gbkCodec = QTextCodec::codecForName("GBK");
    ping = new QProcess(this);
    connect(ping, SIGNAL(started()), this, SLOT(verifyStatus()));
    connect(ping, SIGNAL(finished(int)), this, SLOT(readResult()));
}

PingModel::~PingModel(){
    gbkCodec = nullptr;
}

void PingModel::verifyStatus(){
    if(ping->isReadable()){
        qDebug() << "read on ...";
        connect(ping, SIGNAL(readyRead()), this, SLOT(readResult()));
        if(ping->canReadLine()){
            qDebug() << "LINE read on ...";
        }
    }else{
        qDebug() << "not able to read ...";
    }
}

void PingModel::readResult(){
    running = false;

    QString result = gbkCodec->toUnicode(ping->readLine());
    qDebug() << result;
}

void PingModel::start_command(QString host){
    if(ping){
        QString command = "ping";
        QStringList args;
        args << "-w" <<  "3" <<  host << "-t";
        ping->start(command, args);
        ping->waitForStarted(7000);
        running = true;
        ping->waitForFinished(1000*30);
    }
}

bool PingModel::is_running(){
    return running;
}

bool PingModel::finished(){
    return ping->atEnd();
}

void PingModel::exit(int exitCode)
{
    qDebug()<<"Exiting..................";

    ping->close();
    ping->waitForFinished(0);
    ping->terminate();
    ping->kill();

    QString c = "taskkill /im ping.exe /f";
    int pInt = QProcess::execute(c,{});
    qDebug()<<"pInt:"<<pInt;
}
