﻿#ifndef PINGMODEL_H
#define PINGMODEL_H

#include <QObject>
#include <QProcess>
#include <QTextCodec>

class PingModel : public QObject
{
    Q_OBJECT
public:
    explicit PingModel(QObject *parent = 0);
    ~PingModel();

    void start_command(QString host);
    bool is_running();
    bool finished();
    void exit(int exitCode=0);

signals:

public slots:
    void verifyStatus();
    void readResult();

private:
    QProcess *ping;
    bool running;
    QTextCodec *gbkCodec;
};

#endif // PINGMODEL_H
